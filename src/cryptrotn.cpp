#include <iostream>
#include <string>
#include <cstdlib>
#include <cmath>
/*COLOR*/
void red()
{
  std::system("tput setaf 1");
}

void load()
{
  std::system("tput setaf 4");
}

void green()
{
  std::system("tput setaf 2");
}
void reset()
{
  std::system("tput sgr0");
}

int main(int argc, char *argv[])
{
  argc = argc;
  std::string str = argv[2];
  red();
  std::cout << " Input Text : \n\n";
  std::cout << str  << std::endl;
  reset();
  unsigned int rot = std::stoi(argv[1]);

  std::cout << "\nRunning Decrypting process  ";

  for (unsigned int i = 0; i < str.size(); i++)
  {
    int temp = str[i] + rot;
    if ((i+1) % 10 == 0)
    {
      red();
      std::cout << "==";
    }

    if (str[i] >= 65)
    {
      if (temp % 122 == temp)
      {
        str[i] = str[i] + rot;
      }
        if (temp % 122 == temp && temp > 122)
      {
        str[i] = 'a' + (temp - 122) - 1;
      }
      if (temp % 122 == temp && temp > 65 && temp < 97)
      {
        str[i] = 'A' + (temp - 90) - 1;
      }
    }
  }
  reset();
  std::cout << ">  End Encrypting Process\n\n" << std::endl;
  green();
  std::cout << " The Cypher text is : \n";
  std::cout << str << std::endl;
  std::cout << std::endl;
  reset();
  return 0;
}

