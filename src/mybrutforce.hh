#ifndef MY_BRUT_FORCE_HH
#define MY_BRUT_FORCE_HH

#include <iostream>
#include <cstdlib>
#include <string>
#include <chrono>
#include <ctime>

#include "cpwd.hh"


static const char alphanum[] =
//"0123456789"
//"!@#$%^&*"
//"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

class BrutForce
{
  public:
    /*Constructor*/
    BrutForce()
      : nbtry(0)
    {}

    /*Constructor Overload*/
    BrutForce(int len)
      : nbtry(0), pwdlen(len)
    {}

    char genRandom()
    {
      return alphanum[rand() % stringLength];
    }

    std::string generator()
    {
      srand(time(0));
      std::string Str;
      for(unsigned int i = 0; i < 3; ++i)/*lengthdefault value : 4*/
      {
        Str += genRandom();
      }
      return Str;
    }

    /*COLOR functions*/
    void red()
    {
      std::system("tput setaf 1");/*RED*/
    }
    void green()
    {
      std::system("tput setaf 2");/*GREEN*/
    }

    void reset()
    {
      std::system("tput sgr0");/*RESET*/
    }

    void asklength()
    {
      std::cout << "Please enter length of the PWD's string :" << std::endl;
      std::cout << "Please enter length 0 for default mode." << std::endl;
      std::cin >> pwdlen;
    }

    void trying(Crack c)
    {
      std::string str = "";
      while (true)
      {
        str = generator();
        std::cout <<"dsfsdf" << std::endl;
        std::cout << str << std::endl;
        std::cout << nbtry  << std::endl;
        ++nbtry;
        if (c.check(str))
        {
          c.display_true();
          return;/*succeed*/
        }
        if (nbtry > 99000)
        {
          red();
          std::cout << "Overtime ... Mission failed" << std::endl;
          reset();
          return;
        }
      }
    }

  private:
    int nbtry;
    int pwdlen;
    int stringLength = sizeof(alphanum) - 1;
};

#endif /* !MY_BRUT_FORCE_HH */
