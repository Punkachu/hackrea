#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>

#define PASSWORD "KIKI23"

void display_init()
{
  printf("             ------------------------------\n");
  for (int i = 0; i < 3; i++)
  {
    printf("             |                             |\n");
  }
  printf("             |       ADMINISTRATOR         |\n");
  printf("             |       PASSWORD: ____        |\n");
  printf("             ------------------------------\n");
}

void display_true()
{
  printf("             ------------------------------\n");
  printf("             |       ADMINISTRATOR         |\n");
  for (int i = 0; i < 2; i++)
  {
    printf("             |                             |\n");
  }
  printf("             |                             |\n");
  printf("             |       PASSWORD: KIKI23      |\n");
  printf("             ------------------------------\n");
  printf("RIGHT ARE ALLOWED, LAUCH COMMUNICATION SERVER 5.3.6\n");
}

void display_false()
{
  printf("             ------------------------------\n");
  for (int i = 0; i < 3; i++)
  {
    printf("             |                             |\n");
  }
  printf("             |       PASSWORD: ERROR       |\n");
  for (int i = 0; i < 3; i++)
  {
    printf("             |                             |\n");
  }
  printf("             ------------------------------\n");
}

void red()
{
    system("tput setaf 1");/*RED*/
}
void green()
{
    system("tput setaf 2");/*GREEN*/
}

void reset()
{
    system("tput sgr0");/*RESET*/
}

int check (char *str)
{
  if (strcmp(str, PASSWORD) == 0)
  {
    green();
    display_true();
    reset();
    return 1;
  }
  else
  {
    red();
    display_false();
    reset();
    return 0;
  }
}

int check_p (char *str, char *pwd)
{
  if (strcmp(str, pwd) == 0)
  {
    green();
    display_true();
    reset();
    return 1;
  }
  else
  {
    red();
    display_false();
    reset();
    return 0;
  }
}


int main()
{
  int nbtry = 3;
  char strin[20];
  char *pwd = "KOKO";

  display_init();

  while (nbtry)
  {
    if (nbtry == 3)
    {
      printf("Please enter the password\n");
    }
    else
    {
      printf("The password doesn't match, remain %d try.\n", nbtry);
    }
    scanf("%s", strin);
    if (check_p(strin, pwd))
      return 0;
    else
      --nbtry;
  }
  printf("\n\n\n");
  red();
  printf("Access deny, enable to connect, exit program now --->\n");
  reset();
  return 1;
}
